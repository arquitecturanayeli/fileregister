LIBRARY ieee;
LIBRARY STD;
USE STD.TEXTIO.ALL;
USE ieee.std_logic_TEXTIO.ALL;	--PERMITE USAR STD_LOGIC 

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_UNSIGNED.ALL;
USE ieee.std_logic_ARITH.ALL;
ENTITY Test1 IS
END Test1;
 
ARCHITECTURE behavior OF Test1 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ArchivoDeRegistro
    PORT(
         d : IN  std_logic_vector(15 downto 0);
         read_data1 : OUT  std_logic_vector(15 downto 0);
         read_data2 : OUT  std_logic_vector(15 downto 0);
         clk : IN  std_logic;
         clr : IN  std_logic;
         wr : IN  std_logic;
         she : IN  std_logic;
         dir : IN  std_logic;
         read_register1 : IN  std_logic_vector(3 downto 0);
         read_register2 : IN  std_logic_vector(3 downto 0);
         shamt : IN  std_logic_vector(3 downto 0);
         wreg : IN  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';
   signal wr : std_logic := '0';
   signal she : std_logic := '0';
   signal dir : std_logic := '0';
   signal read_register1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_register2 : std_logic_vector(3 downto 0) := (others => '0');
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');
   signal wreg : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal read_data1 : std_logic_vector(15 downto 0);
   signal read_data2 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ArchivoDeRegistro PORT MAP (
          d => d,
          read_data1 => read_data1,
          read_data2 => read_data2,
          clk => clk,
          clr => clr,
          wr => wr,
          she => she,
          dir => dir,
          read_register1 => read_register1,
          read_register2 => read_register2,
          shamt => shamt,
          wreg => wreg
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	
	--variables de archivo
	file ARCH_RES :  TEXT;	--archivo de salida
	file ARCH_ENT : TEXT; --archivo de entrada	
	
	variable LINEA_RES : line;
	variable LINEA_ENT : line;
	
	-- variables de entrada

   variable var_d : std_logic_vector(15 downto 0) := (others => '0');

   variable var_clr : std_logic := '0';
   variable var_wr : std_logic := '0';
   variable var_she : std_logic := '0';
   variable var_dir : std_logic := '0';
   variable var_read_register1 : std_logic_vector(3 downto 0) := (others => '0');
   variable var_read_register2 : std_logic_vector(3 downto 0) := (others => '0');
   variable var_shamt : std_logic_vector(3 downto 0) := (others => '0');
   variable var_wreg : std_logic_vector(3 downto 0) := (others => '0');

 	--variables de salida
   variable var_read_data1 : std_logic_vector(15 downto 0);
   variable var_read_data2 : std_logic_vector(15 downto 0);
	VARIABLE CADENA : STRING(1 TO 5);
	
   begin		
      file_open(ARCH_ENT, "ENTRADAS1.txt", READ_MODE); 	
		file_open(ARCH_RES, "RESULTADO.txt", WRITE_MODE); 
		
		CADENA := "  RR1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "RD1"
		CADENA := "  RR2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   RR2"
		CADENA := "SHAMT";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA " SHAMT"
		CADENA := " WREG";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   WREG"
		CADENA := "   WD";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   WD"
		CADENA := "   WR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   WR"
		CADENA := "  SHE";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   SHE"
		CADENA := "  DIR";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   DIR"
		CADENA := "  RD1";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   RD1"
		CADENA := "  RD2";
		write(LINEA_RES, CADENA, right, CADENA'LENGTH+1);	--ESCRIBE LA CADENA "   RD2"
		writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo

		WAIT FOR 100 NS;
		
		WHILE NOT ENDFILE(ARCH_ENT) LOOP
			readline(ARCH_ENT,LINEA_ENT); -- lee una linea completa del archivo ARCH_ENT
							--y lo guarda en LINEA_ENT

			read(LINEA_ENT, var_read_register1);
			read_register1 <= var_read_register1;
			
			read(LINEA_ENT, var_read_register2);
			read_register2 <= var_read_register2;
			
			read(LINEA_ENT, var_shamt);
			shamt <= var_shamt;
			
			read(LINEA_ENT, var_wreg);
			wreg <= var_wreg;
			
			Hread(LINEA_ENT, var_d);
			d <= var_d;
			
			read(LINEA_ENT, var_wr);
			wr <= var_wr;
			
			read(LINEA_ENT, var_she);
			she <= var_she;
			
			read(LINEA_ENT, var_dir);
			dir <= var_dir;
			
			read(LINEA_ENT, var_clr);
			clr <= var_clr;
			
			WAIT UNTIL RISING_EDGE(CLK);	--ESPERO AL FLANCO DE SUBIDA 

			var_read_data1 := read_data1;
			var_read_data2 := read_data2;
			

			Hwrite(LINEA_RES, var_read_register1, right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES,var_read_register2, 	right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_shamt, 	right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_wreg, 	right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_d, 	right, 5);	--ESCRIBE EL CAMPO 
			write(LINEA_RES, var_wr, 	right, 5);	--ESCRIBE EL CAMPO 
			write(LINEA_RES, var_she, 	right, 5);	--ESCRIBE EL CAMPO 
			write(LINEA_RES, var_dir, 	right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_read_data1, 	right, 5);	--ESCRIBE EL CAMPO 
			Hwrite(LINEA_RES, var_read_data2, 	right, 5);	--ESCRIBE EL CAMPO 

			writeline(ARCH_RES,LINEA_RES);-- escribe la linea en el archivo
			
		end loop;
		file_close(ARCH_ENT);  -- cierra el archivo
		file_close(ARCH_RES);  -- cierra el archivo
		
   end process;

END;
