-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:27:27 09/25/2016 
-- Design Name: 
-- Module Name:    Archivo_Registros - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity ArchivoDeRegistro is
	generic(
		bus_dir : integer := 4;
		tam_palabra : integer := 16
	);
    Port ( d : in  STD_LOGIC_VECTOR (tam_palabra-1 downto 0);
           read_data1, read_data2 : out  STD_LOGIC_VECTOR (tam_palabra-1 downto 0);
           clk, clr, wr, she ,dir : in  STD_LOGIC;
			  read_register1, read_register2 : std_logic_vector(bus_dir-1 downto 0);
			  shamt : in  STD_LOGIC_VECTOR (bus_dir-1 downto 0);
           wreg : in  STD_LOGIC_VECTOR (bus_dir-1 downto 0));
end ArchivoDeRegistro;

architecture Behavioral of ArchivoDeRegistro is
type registros is array (0 to (2**bus_dir)-1) of std_logic_vector(tam_palabra-1 downto 0);
signal arreglo : registros;

begin

	process (clk ,clr,d)
	variable aux: std_logic_vector((2**bus_dir)-1 downto 0);
	begin	
		if (clr = '1') then
			for i in 0 to (2**bus_dir)-1 loop
				arreglo(i) <= (others => '0');
			end loop;
		elsif(rising_edge(clk)) then
			if (wr = '1') then
				if(she = '0') then--Cargar dato
					arreglo(conv_integer(wreg)) <= d;					
				else--Corrimientos
				aux:=arreglo(conv_integer(wreg));
						for i in 0 to bus_dir-1 loop
							if(dir = '1')then--Corrimiento izq
								for j in (2**bus_dir)-1 downto 0 loop
										if (shamt(i)='1') then 
											if((j-(2**i))<0) then 
												aux(j):='0';
											else 
												aux(j):= aux(j-(2**i));
											end if;
										else 
											aux(j):= aux(j);
										end if;			
								end loop;
							else--corrimiento derecha
								for j in 0 to (2**bus_dir)-1 loop
									if (shamt(i)='1') then 
										if((j+(2**i))>(2**bus_dir)-1) then 
											aux(j):='0';
										else 
											aux(j):= aux(j+(2**i));
										end if;
									else 
										aux(j):= aux(j);
									end if;			
								end loop;
							end if;
						end loop;						
						arreglo(conv_integer(wreg))<= aux;					
				end if;
			end if;
		end if;
	end process;
	read_data1<=arreglo(conv_integer(read_register1));
	read_data2<=arreglo(conv_integer(read_register2));

	

end Behavioral;