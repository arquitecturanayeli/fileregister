/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/juancarlos/Documentos/escom/7moSemestre/arquitectura/2Parcial/P4ArchivoDeRegistrosCar/P4FileRegister/fileregister/Test1.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );
void ieee_p_3564397177_sub_1675032430035817134_91900896(char *, char *, char *, char *);
void ieee_p_3564397177_sub_2258168291854845616_91900896(char *, char *, char *, char *, char *, unsigned char , int );
void ieee_p_3564397177_sub_2518136782373626985_91900896(char *, char *, char *, unsigned char , unsigned char , int );
void ieee_p_3564397177_sub_2863756418437601506_91900896(char *, char *, char *, char *, char *);
void ieee_p_3564397177_sub_2863978933202788330_91900896(char *, char *, char *, char *, char *);


static void work_a_2221974671_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 5872U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 6520);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(75, ng0);
    t2 = (t0 + 3088U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 5680);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 6520);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(77, ng0);
    t2 = (t0 + 3088U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 5680);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_2221974671_2372691052_p_1(char *t0)
{
    char t5[16];
    char t10[8];
    char t11[8];
    char t12[8];
    char t13[8];
    char t14[8];
    char t15[8];
    char t16[8];
    char t17[8];
    char t18[8];
    char t19[8];
    char t24[8];
    char t25[8];
    char t26[8];
    char t27[8];
    char t28[16];
    char t29[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    int64 t20;
    unsigned char t21;
    unsigned char t22;
    char *t23;

LAB0:    t1 = (t0 + 6120U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(110, ng0);
    t2 = (t0 + 4840U);
    t3 = (t0 + 14009);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(111, ng0);
    t2 = (t0 + 4736U);
    t3 = (t0 + 14022);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 13;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (13 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)1);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 14035);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(114, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t10, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t10, t6, (unsigned char)0, t8);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 14040);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(116, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t11, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t11, t6, (unsigned char)0, t8);
    xsi_set_current_line(117, ng0);
    t2 = (t0 + 14045);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t12, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t12, t6, (unsigned char)0, t8);
    xsi_set_current_line(119, ng0);
    t2 = (t0 + 14050);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(120, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t13, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t13, t6, (unsigned char)0, t8);
    xsi_set_current_line(121, ng0);
    t2 = (t0 + 14055);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(122, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t14, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t14, t6, (unsigned char)0, t8);
    xsi_set_current_line(123, ng0);
    t2 = (t0 + 14060);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(124, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t15, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t15, t6, (unsigned char)0, t8);
    xsi_set_current_line(125, ng0);
    t2 = (t0 + 14065);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(126, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t16, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t16, t6, (unsigned char)0, t8);
    xsi_set_current_line(127, ng0);
    t2 = (t0 + 14070);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(128, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t17, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t17, t6, (unsigned char)0, t8);
    xsi_set_current_line(129, ng0);
    t2 = (t0 + 14075);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(130, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t18, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t18, t6, (unsigned char)0, t8);
    xsi_set_current_line(131, ng0);
    t2 = (t0 + 14080);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    t6 = (t7 + 0);
    memcpy(t6, t2, 5U);
    xsi_set_current_line(132, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 5296U);
    t6 = (t4 + 56U);
    t7 = *((char **)t6);
    memcpy(t19, t7, 5U);
    t6 = (t0 + 13688U);
    t8 = (5U + 1);
    std_textio_write7(STD_TEXTIO, t2, t3, t19, t6, (unsigned char)0, t8);
    xsi_set_current_line(133, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 4736U);
    t4 = (t0 + 5016U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    xsi_set_current_line(135, ng0);
    t20 = (100 * 1000LL);
    t2 = (t0 + 5928);
    xsi_process_wait(t2, t20);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(137, ng0);

LAB8:    t2 = (t0 + 4840U);
    t21 = std_textio_endfile(t2);
    t22 = (!(t21));
    if (t22 != 0)
        goto LAB9;

LAB11:    xsi_set_current_line(188, ng0);
    t2 = (t0 + 4840U);
    std_textio_file_close(t2);
    xsi_set_current_line(189, ng0);
    t2 = (t0 + 4736U);
    std_textio_file_close(t2);
    goto LAB2;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(138, ng0);
    t3 = (t0 + 5928);
    t4 = (t0 + 4840U);
    t6 = (t0 + 5088U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(141, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3808U);
    t6 = *((char **)t4);
    t4 = (t0 + 13592U);
    ieee_p_3564397177_sub_2863756418437601506_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(142, ng0);
    t2 = (t0 + 3808U);
    t3 = *((char **)t2);
    t2 = (t0 + 6584);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    memcpy(t23, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(144, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3928U);
    t6 = *((char **)t4);
    t4 = (t0 + 13608U);
    ieee_p_3564397177_sub_2863756418437601506_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(145, ng0);
    t2 = (t0 + 3928U);
    t3 = *((char **)t2);
    t2 = (t0 + 6648);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    memcpy(t23, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(147, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 4048U);
    t6 = *((char **)t4);
    t4 = (t0 + 13624U);
    ieee_p_3564397177_sub_2863756418437601506_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(148, ng0);
    t2 = (t0 + 4048U);
    t3 = *((char **)t2);
    t2 = (t0 + 6712);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    memcpy(t23, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(150, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 4168U);
    t6 = *((char **)t4);
    t4 = (t0 + 13640U);
    ieee_p_3564397177_sub_2863756418437601506_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(151, ng0);
    t2 = (t0 + 4168U);
    t3 = *((char **)t2);
    t2 = (t0 + 6776);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    memcpy(t23, t3, 4U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(153, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3208U);
    t6 = *((char **)t4);
    t4 = (t0 + 13576U);
    ieee_p_3564397177_sub_2863978933202788330_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(154, ng0);
    t2 = (t0 + 3208U);
    t3 = *((char **)t2);
    t2 = (t0 + 6840);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    memcpy(t23, t3, 16U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(156, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3448U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_1675032430035817134_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(157, ng0);
    t2 = (t0 + 3448U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t2 = (t0 + 6904);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t21;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(159, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3568U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_1675032430035817134_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(160, ng0);
    t2 = (t0 + 3568U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t2 = (t0 + 6968);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t21;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(162, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3688U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_1675032430035817134_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(163, ng0);
    t2 = (t0 + 3688U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t2 = (t0 + 7032);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t21;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(165, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5088U);
    t4 = (t0 + 3328U);
    t6 = *((char **)t4);
    t4 = (t6 + 0);
    ieee_p_3564397177_sub_1675032430035817134_91900896(IEEE_P_3564397177, t2, t3, t4);
    xsi_set_current_line(166, ng0);
    t2 = (t0 + 3328U);
    t3 = *((char **)t2);
    t21 = *((unsigned char *)t3);
    t2 = (t0 + 7096);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t23 = *((char **)t7);
    *((unsigned char *)t23) = t21;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(168, ng0);

LAB14:    t2 = (t0 + 6440);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:;
LAB12:    t4 = (t0 + 6440);
    *((int *)t4) = 0;
    xsi_set_current_line(170, ng0);
    t2 = (t0 + 2632U);
    t3 = *((char **)t2);
    t2 = (t0 + 4288U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(171, ng0);
    t2 = (t0 + 2792U);
    t3 = *((char **)t2);
    t2 = (t0 + 4408U);
    t4 = *((char **)t2);
    t2 = (t4 + 0);
    memcpy(t2, t3, 16U);
    xsi_set_current_line(174, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3808U);
    t6 = *((char **)t4);
    memcpy(t24, t6, 4U);
    t4 = (t0 + 13592U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t24, t4, (unsigned char)0, 5);
    xsi_set_current_line(175, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3928U);
    t6 = *((char **)t4);
    memcpy(t25, t6, 4U);
    t4 = (t0 + 13608U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t25, t4, (unsigned char)0, 5);
    xsi_set_current_line(176, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 4048U);
    t6 = *((char **)t4);
    memcpy(t26, t6, 4U);
    t4 = (t0 + 13624U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t26, t4, (unsigned char)0, 5);
    xsi_set_current_line(177, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 4168U);
    t6 = *((char **)t4);
    memcpy(t27, t6, 4U);
    t4 = (t0 + 13640U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t27, t4, (unsigned char)0, 5);
    xsi_set_current_line(178, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3208U);
    t6 = *((char **)t4);
    memcpy(t5, t6, 16U);
    t4 = (t0 + 13576U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t5, t4, (unsigned char)0, 5);
    xsi_set_current_line(179, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3448U);
    t6 = *((char **)t4);
    t21 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_2518136782373626985_91900896(IEEE_P_3564397177, t2, t3, t21, (unsigned char)0, 5);
    xsi_set_current_line(180, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3568U);
    t6 = *((char **)t4);
    t21 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_2518136782373626985_91900896(IEEE_P_3564397177, t2, t3, t21, (unsigned char)0, 5);
    xsi_set_current_line(181, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 3688U);
    t6 = *((char **)t4);
    t21 = *((unsigned char *)t6);
    ieee_p_3564397177_sub_2518136782373626985_91900896(IEEE_P_3564397177, t2, t3, t21, (unsigned char)0, 5);
    xsi_set_current_line(182, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 4288U);
    t6 = *((char **)t4);
    memcpy(t28, t6, 16U);
    t4 = (t0 + 13656U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t28, t4, (unsigned char)0, 5);
    xsi_set_current_line(183, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 5016U);
    t4 = (t0 + 4408U);
    t6 = *((char **)t4);
    memcpy(t29, t6, 16U);
    t4 = (t0 + 13672U);
    ieee_p_3564397177_sub_2258168291854845616_91900896(IEEE_P_3564397177, t2, t3, t29, t4, (unsigned char)0, 5);
    xsi_set_current_line(185, ng0);
    t2 = (t0 + 5928);
    t3 = (t0 + 4736U);
    t4 = (t0 + 5016U);
    std_textio_writeline(STD_TEXTIO, t2, t3, t4);
    goto LAB8;

LAB13:    t3 = (t0 + 1152U);
    t21 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t3, 0U, 0U);
    if (t21 == 1)
        goto LAB12;
    else
        goto LAB14;

LAB15:    goto LAB13;

}


extern void work_a_2221974671_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2221974671_2372691052_p_0,(void *)work_a_2221974671_2372691052_p_1};
	xsi_register_didat("work_a_2221974671_2372691052", "isim/Test1_isim_beh.exe.sim/work/a_2221974671_2372691052.didat");
	xsi_register_executes(pe);
}
