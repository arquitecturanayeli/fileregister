/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/juancarlos/Documentos/escom/7moSemestre/arquitectura/2Parcial/P4ArchivoDeRegistrosCar/P4FileRegister/fileregister/ArchivoDeRegistro.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_5109402382352621412_3965413181(char *, char *, char *);


static void work_a_1419192429_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    int t5;
    int t6;
    char *t7;
    int t8;
    int t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    unsigned char t23;
    unsigned char t24;
    unsigned char t25;
    int t26;
    int t27;
    int t28;
    int t29;
    int t30;
    int t31;
    char *t32;
    int t33;
    int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    char *t38;
    int t39;

LAB0:    xsi_set_current_line(46, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1472U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB10;

LAB11:
LAB3:    t1 = (t0 + 5280);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(47, ng0);
    t5 = xsi_vhdl_pow(2, 4);
    t6 = (t5 - 1);
    t1 = (t0 + 10821);
    *((int *)t1) = 0;
    t7 = (t0 + 10825);
    *((int *)t7) = t6;
    t8 = 0;
    t9 = t6;

LAB5:    if (t8 <= t9)
        goto LAB6;

LAB8:    goto LAB3;

LAB6:    xsi_set_current_line(48, ng0);
    t10 = xsi_get_transient_memory(16U);
    memset(t10, 0, 16U);
    t11 = t10;
    memset(t11, (unsigned char)2, 16U);
    t12 = (t0 + 10821);
    t13 = *((int *)t12);
    t14 = (t13 - 0);
    t15 = (t14 * 1);
    t16 = (16U * t15);
    t17 = (0U + t16);
    t18 = (t0 + 5392);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t10, 16U);
    xsi_driver_first_trans_delta(t18, t17, 16U, 0LL);

LAB7:    t1 = (t0 + 10821);
    t8 = *((int *)t1);
    t2 = (t0 + 10825);
    t9 = *((int *)t2);
    if (t8 == t9)
        goto LAB8;

LAB9:    t5 = (t8 + 1);
    t8 = t5;
    t7 = (t0 + 10821);
    *((int *)t7) = t8;
    goto LAB5;

LAB10:    xsi_set_current_line(51, ng0);
    t2 = (t0 + 1832U);
    t7 = *((char **)t2);
    t4 = *((unsigned char *)t7);
    t23 = (t4 == (unsigned char)3);
    if (t23 != 0)
        goto LAB12;

LAB14:
LAB13:    goto LAB3;

LAB12:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 1992U);
    t10 = *((char **)t2);
    t24 = *((unsigned char *)t10);
    t25 = (t24 == (unsigned char)2);
    if (t25 != 0)
        goto LAB15;

LAB17:    xsi_set_current_line(55, ng0);
    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 2792U);
    t7 = *((char **)t1);
    t1 = (t0 + 10432U);
    t5 = ieee_p_3620187407_sub_5109402382352621412_3965413181(IEEE_P_3620187407, t7, t1);
    t6 = (t5 - 0);
    t15 = (t6 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t5);
    t16 = (16U * t15);
    t17 = (0 + t16);
    t10 = (t2 + t17);
    t11 = (t0 + 3488U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t10, 16U);
    xsi_set_current_line(56, ng0);
    t5 = (4 - 1);
    t1 = (t0 + 10829);
    *((int *)t1) = 0;
    t2 = (t0 + 10833);
    *((int *)t2) = t5;
    t6 = 0;
    t8 = t5;

LAB18:    if (t6 <= t8)
        goto LAB19;

LAB21:    xsi_set_current_line(83, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 2792U);
    t7 = *((char **)t1);
    t1 = (t0 + 10432U);
    t5 = ieee_p_3620187407_sub_5109402382352621412_3965413181(IEEE_P_3620187407, t7, t1);
    t6 = (t5 - 0);
    t15 = (t6 * 1);
    t16 = (16U * t15);
    t17 = (0U + t16);
    t10 = (t0 + 5392);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t18 = (t12 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t2, 16U);
    xsi_driver_first_trans_delta(t10, t17, 16U, 0LL);

LAB16:    goto LAB13;

LAB15:    xsi_set_current_line(53, ng0);
    t2 = (t0 + 1032U);
    t11 = *((char **)t2);
    t2 = (t0 + 2792U);
    t12 = *((char **)t2);
    t2 = (t0 + 10432U);
    t5 = ieee_p_3620187407_sub_5109402382352621412_3965413181(IEEE_P_3620187407, t12, t2);
    t6 = (t5 - 0);
    t15 = (t6 * 1);
    t16 = (16U * t15);
    t17 = (0U + t16);
    t18 = (t0 + 5392);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t11, 16U);
    xsi_driver_first_trans_delta(t18, t17, 16U, 0LL);
    goto LAB16;

LAB19:    xsi_set_current_line(57, ng0);
    t7 = (t0 + 2152U);
    t10 = *((char **)t7);
    t3 = *((unsigned char *)t10);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB22;

LAB24:    xsi_set_current_line(70, ng0);
    t5 = xsi_vhdl_pow(2, 4);
    t9 = (t5 - 1);
    t1 = (t0 + 10845);
    *((int *)t1) = 0;
    t2 = (t0 + 10849);
    *((int *)t2) = t9;
    t13 = 0;
    t14 = t9;

LAB36:    if (t13 <= t14)
        goto LAB37;

LAB39:
LAB23:
LAB20:    t1 = (t0 + 10829);
    t6 = *((int *)t1);
    t2 = (t0 + 10833);
    t8 = *((int *)t2);
    if (t6 == t8)
        goto LAB21;

LAB47:    t5 = (t6 + 1);
    t6 = t5;
    t7 = (t0 + 10829);
    *((int *)t7) = t6;
    goto LAB18;

LAB22:    xsi_set_current_line(58, ng0);
    t9 = xsi_vhdl_pow(2, 4);
    t13 = (t9 - 1);
    t7 = (t0 + 10837);
    *((int *)t7) = t13;
    t11 = (t0 + 10841);
    *((int *)t11) = 0;
    t14 = t13;
    t26 = 0;

LAB25:    if (t14 >= t26)
        goto LAB26;

LAB28:    goto LAB23;

LAB26:    xsi_set_current_line(59, ng0);
    t12 = (t0 + 2632U);
    t18 = *((char **)t12);
    t12 = (t0 + 10829);
    t27 = *((int *)t12);
    t28 = (t27 - 3);
    t15 = (t28 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t12));
    t16 = (1U * t15);
    t17 = (0 + t16);
    t19 = (t18 + t17);
    t23 = *((unsigned char *)t19);
    t24 = (t23 == (unsigned char)3);
    if (t24 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 10837);
    t5 = *((int *)t1);
    t9 = (t5 - 15);
    t15 = (t9 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t16 = (1U * t15);
    t17 = (0 + t16);
    t7 = (t2 + t17);
    t3 = *((unsigned char *)t7);
    t10 = (t0 + 3488U);
    t11 = *((char **)t10);
    t10 = (t0 + 10837);
    t13 = *((int *)t10);
    t27 = (t13 - 15);
    t35 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t10));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t12 = (t11 + t37);
    *((unsigned char *)t12) = t3;

LAB30:
LAB27:    t1 = (t0 + 10837);
    t14 = *((int *)t1);
    t2 = (t0 + 10841);
    t26 = *((int *)t2);
    if (t14 == t26)
        goto LAB28;

LAB35:    t5 = (t14 + -1);
    t14 = t5;
    t7 = (t0 + 10837);
    *((int *)t7) = t14;
    goto LAB25;

LAB29:    xsi_set_current_line(60, ng0);
    t20 = (t0 + 10837);
    t21 = (t0 + 10829);
    t29 = xsi_vhdl_pow(2, *((int *)t21));
    t30 = *((int *)t20);
    t31 = (t30 - t29);
    t25 = (t31 < 0);
    if (t25 != 0)
        goto LAB32;

LAB34:    xsi_set_current_line(63, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 10837);
    t7 = (t0 + 10829);
    t5 = xsi_vhdl_pow(2, *((int *)t7));
    t9 = *((int *)t1);
    t13 = (t9 - t5);
    t27 = (t13 - 15);
    t15 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t13);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t2 + t17);
    t3 = *((unsigned char *)t10);
    t11 = (t0 + 3488U);
    t12 = *((char **)t11);
    t11 = (t0 + 10837);
    t28 = *((int *)t11);
    t29 = (t28 - 15);
    t35 = (t29 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t11));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t18 = (t12 + t37);
    *((unsigned char *)t18) = t3;

LAB33:    goto LAB30;

LAB32:    xsi_set_current_line(61, ng0);
    t22 = (t0 + 3488U);
    t32 = *((char **)t22);
    t22 = (t0 + 10837);
    t33 = *((int *)t22);
    t34 = (t33 - 15);
    t35 = (t34 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t22));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t38 = (t32 + t37);
    *((unsigned char *)t38) = (unsigned char)2;
    goto LAB33;

LAB37:    xsi_set_current_line(71, ng0);
    t7 = (t0 + 2632U);
    t10 = *((char **)t7);
    t7 = (t0 + 10829);
    t26 = *((int *)t7);
    t27 = (t26 - 3);
    t15 = (t27 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t7));
    t16 = (1U * t15);
    t17 = (0 + t16);
    t11 = (t10 + t17);
    t3 = *((unsigned char *)t11);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB40;

LAB42:    xsi_set_current_line(78, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 10845);
    t5 = *((int *)t1);
    t9 = (t5 - 15);
    t15 = (t9 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t16 = (1U * t15);
    t17 = (0 + t16);
    t7 = (t2 + t17);
    t3 = *((unsigned char *)t7);
    t10 = (t0 + 3488U);
    t11 = *((char **)t10);
    t10 = (t0 + 10845);
    t26 = *((int *)t10);
    t27 = (t26 - 15);
    t35 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t10));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t12 = (t11 + t37);
    *((unsigned char *)t12) = t3;

LAB41:
LAB38:    t1 = (t0 + 10845);
    t13 = *((int *)t1);
    t2 = (t0 + 10849);
    t14 = *((int *)t2);
    if (t13 == t14)
        goto LAB39;

LAB46:    t5 = (t13 + 1);
    t13 = t5;
    t7 = (t0 + 10845);
    *((int *)t7) = t13;
    goto LAB36;

LAB40:    xsi_set_current_line(72, ng0);
    t12 = (t0 + 10845);
    t18 = (t0 + 10829);
    t28 = xsi_vhdl_pow(2, *((int *)t18));
    t29 = *((int *)t12);
    t30 = (t29 + t28);
    t31 = xsi_vhdl_pow(2, 4);
    t33 = (t31 - 1);
    t23 = (t30 > t33);
    if (t23 != 0)
        goto LAB43;

LAB45:    xsi_set_current_line(75, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 10845);
    t7 = (t0 + 10829);
    t5 = xsi_vhdl_pow(2, *((int *)t7));
    t9 = *((int *)t1);
    t26 = (t9 + t5);
    t27 = (t26 - 15);
    t15 = (t27 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t26);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t2 + t17);
    t3 = *((unsigned char *)t10);
    t11 = (t0 + 3488U);
    t12 = *((char **)t11);
    t11 = (t0 + 10845);
    t28 = *((int *)t11);
    t29 = (t28 - 15);
    t35 = (t29 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t11));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t18 = (t12 + t37);
    *((unsigned char *)t18) = t3;

LAB44:    goto LAB41;

LAB43:    xsi_set_current_line(73, ng0);
    t19 = (t0 + 3488U);
    t20 = *((char **)t19);
    t19 = (t0 + 10845);
    t34 = *((int *)t19);
    t39 = (t34 - 15);
    t35 = (t39 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t19));
    t36 = (1U * t35);
    t37 = (0 + t36);
    t21 = (t20 + t37);
    *((unsigned char *)t21) = (unsigned char)2;
    goto LAB44;

}

static void work_a_1419192429_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(88, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 2312U);
    t3 = *((char **)t1);
    t1 = (t0 + 10384U);
    t4 = ieee_p_3620187407_sub_5109402382352621412_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5456);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5296);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1419192429_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(89, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 2472U);
    t3 = *((char **)t1);
    t1 = (t0 + 10400U);
    t4 = ieee_p_3620187407_sub_5109402382352621412_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 0);
    t6 = (t5 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5520);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5312);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1419192429_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1419192429_3212880686_p_0,(void *)work_a_1419192429_3212880686_p_1,(void *)work_a_1419192429_3212880686_p_2};
	xsi_register_didat("work_a_1419192429_3212880686", "isim/Test1_isim_beh.exe.sim/work/a_1419192429_3212880686.didat");
	xsi_register_executes(pe);
}
