----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:09:48 09/25/2016 
-- Design Name: 
-- Module Name:    BarrelShifter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BarrelShifter is
	generic(
		bus_dir : integer := 4;
		tam_palabra : integer := 16
	);
    Port ( d : in  STD_LOGIC_VECTOR (tam_palabra-1 downto 0);
           q : out  STD_LOGIC_VECTOR (tam_palabra-1 downto 0);
			  dir: in STD_LOGIC;
           shamt : in  STD_LOGIC_VECTOR (bus_dir-1 downto 0));
end BarrelShifter;

architecture Behavioral of BarrelShifter is

begin

process(d,shamt,dir)
variable aux: std_logic_vector((2**bus_dir)-1 downto 0);
begin
	aux:=d;	
	for i in 0 to bus_dir-1 loop
		if(dir = '1')then--Corrimiento izq
			for j in (2**bus_dir)-1 downto 0 loop
					if (shamt(i)='1') then --corrimiento
						if((j-(2**i))<0) then 
							aux(j):='0';
						else 
							aux(j):= aux(j-(2**i));
						end if;
					else 
						aux(j):= aux(j);
					end if;			
			end loop;
		else--corrimiento derecha
			for j in 0 to (2**bus_dir)-1 loop
				if (shamt(i)='1') then --corrimiento
					if((j+(2**i))>(2**bus_dir)-1) then 
						aux(j):='0';
					else 
						aux(j):= aux(j+(2**i));
					end if;
				else 
					aux(j):= aux(j);
				end if;			
			end loop;
		end if;
	end loop;
	
	q<= aux;
end process;

end Behavioral;